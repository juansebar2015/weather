//
//  AutocompleteResultVC.swift
//  Weather
//
//  Created by Juan Ramirez on 9/9/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class AutocompleteResultVC: GMSAutocompleteResultsViewController, UISearchDisplayDelegate, GMSAutocompleteTableDataSourceDelegate {
    
    var citySelected : String!
    var latitude: String!
    var longitude: String!
    
    var searchBar: UISearchBar?
    var tableDataSource: GMSAutocompleteTableDataSource?
    var srchDisplayController: UISearchDisplayController?
    
//    var resultsViewController: GMSAutocompleteResultsViewController?
//    var searchController: UISearchController?
//    var resultView: UITextView?
    
    //@IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar = UISearchBar(frame: CGRectMake(0, 74, self.view.frame.width, 44.0))
        
        tableDataSource = GMSAutocompleteTableDataSource()
        tableDataSource?.delegate = self
        
        srchDisplayController = UISearchDisplayController(searchBar: searchBar!, contentsController: self)
        srchDisplayController?.searchResultsDataSource = tableDataSource
        srchDisplayController?.searchResultsDelegate = tableDataSource
        srchDisplayController?.delegate = self
        
        self.view.addSubview(searchBar!)
        
    }
    
    @IBAction func onBackButtonPress(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
 
    // Reload the table with new data based on final users input
    func didUpdateAutocompletePredictionsForTableDataSource(tableDataSource: GMSAutocompleteTableDataSource) {
        // Turn the network activity indicator off.
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        // Reload table data.
        srchDisplayController?.searchResultsTableView.reloadData()
    }
    
    // Detects user input in searchBar
    func didRequestAutocompletePredictionsForTableDataSource(tableDataSource: GMSAutocompleteTableDataSource) {
        // Turn the network activity indicator on.
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        // Reload table data.
        srchDisplayController?.searchResultsTableView.reloadData()
    }
    
    // Obtain and manage the data from users selection
    func tableDataSource(tableDataSource: GMSAutocompleteTableDataSource, didAutocompleteWithPlace place: GMSPlace) {
        srchDisplayController?.active = false
        
        //citySelected = place
        
        // Do something with the selected place.
        print("Place name: \(place.name)")
        //citySelected = place.name
        
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        
        //latitude = String(place.coordinate.latitude)
        //longitude = String(place.coordinate.longitude)
        
        print("\(citySelected) Coordinates: Lat: \(latitude), Lon: \(longitude)")
        
        performSegueWithIdentifier("returnToMainView", sender: place)
    }
    
    // Reloads table from the new input of user
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String?) -> Bool {
        tableDataSource?.sourceTextHasChanged(searchString)
        return false
    }
    
    func tableDataSource(tableDataSource: GMSAutocompleteTableDataSource, didFailAutocompleteWithError error: NSError) {
        // TODO: Handle the error.
        print("Error: \(error.description)")
    }
    
    // User selects a row from table
    func tableDataSource(tableDataSource: GMSAutocompleteTableDataSource, didSelectPrediction prediction: GMSAutocompletePrediction) -> Bool {
        return true
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "returnToMainView" {
            if let weatherVC = segue.destinationViewController as? ViewController{

                if let city = sender as? GMSPlace {
                    weatherVC.cityName = city.name
                    weatherVC.longitude = String(city.coordinate.longitude)
                    weatherVC.latitude = String(city.coordinate.latitude)
                    weatherVC.userEnteredNewCity = true
                }
            }
        }
    }
}
