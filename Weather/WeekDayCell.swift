//
//  WeekDayCell.swift
//  Weather
//
//  Created by Juan Ramirez on 9/20/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class WeekDayCell: UICollectionViewCell {
 
    @IBOutlet weak var weekDayLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    
    func configCell(weather: Weather, dayNum: Int) {
        weekDayLabel.text = weather.daysOfWeek[dayNum]
        iconImage.image = UIImage(named: weather.icon[dayNum])
        maxTemp.text = weather.temperatureMax[dayNum]
        minTemp.text = weather.temperatureMin[dayNum]
    }
    
}
