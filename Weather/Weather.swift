//
//  Weather.swift
//  Weather
//
//  Created by Juan Ramirez on 9/8/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import Alamofire

class Weather {
    
    private var _cityName: String!
    private var _longitude: String!    //CLLocationCoordinate2D!
    private var _latitude: String!    //CLLocationCoordinate2D!
    private var _temperature: String!
    private var _temperatureMin: [String]!
    private var _temperatureMax: [String]!
    private var _time: String!
    private var _timezone: String!
    private var _date: String!
    private var _daysOfWeek: [String]!
    private var _tempDescription: String!
    private var _icon: [String]!
    private var _humidity: String!
    private var _windSpeed: String!
    private var _precipProbability: String!
    private var _weatherHTTP: String!
    
    
    
    init(name: String, lat: String, lon: String){
        
        self._cityName = name
        self._latitude = lat
        self._longitude = lon
        self._temperatureMax = []
        self._temperatureMin = []
        self._icon = []
        
        self._weatherHTTP = "\(forecastURL_BASE)\(forecastAPP_KEY)\(self._latitude),\(self._longitude)"
    }
    
    func getWeatherData(completed: DownloadComplete){

        let url = NSURL(string: _weatherHTTP)!    //forecaseSampleCity
        
        
        Alamofire.request(.GET, url).responseJSON{ response in
            let result = response.result
            
            if let dict = result.value as? Dictionary<String, AnyObject> {
                
                if let currentWeather = dict["currently"] as? Dictionary<String,AnyObject> {
                    
                    if let time = currentWeather["time"] as? Double{
                        
                        print(currentWeather.debugDescription)
                        
                        if let timezoneDiff = dict["offset"] as? Int {
                           
                            if let timezone = dict["timezone"] as? String {
                                self._timezone = timezone
                            }
                            
                            let userCalendar = NSCalendar.currentCalendar()
                            let dateComponents = NSDateComponents()
                            dateComponents.timeZone = NSTimeZone(name: self._timezone)
                            
                            var date = userCalendar.componentsInTimeZone(dateComponents.timeZone!, fromDate: NSDate(timeIntervalSince1970: time))
                            
                            let am_pm = self.setupTimeByTimezone(date)
                            self.getWeekDays(date)
                            
                            self._time = "\(date.hour):\(String(format:"%02d",date.minute)) \(am_pm)" //"\(dateDict["hour"]!):\(dateDict["minute"]!) \(dateDict["am_pm"]!)"
                            self._date = "\(date.month)/\(date.day)/\(date.year)" //"\(dateDict["month"]!)/\(dateDict["day"]!)/\(dateDict["year"]!)"
                            
                            print(date.debugDescription)
                            //print(dateString.debugDescription)
                        }
                    }
                    
                    
                    if let iconDes = currentWeather["icon"] as? String {
                        self._icon.append(iconDes)
                    }
                    
                    // Set up current, max, min temperature
                    if let temp = currentWeather["temperature"] as? Double {
                        self._temperature = "\(Int(round(temp)))"
                        
                        if let dailyTemp = dict["daily"] as? Dictionary<String, AnyObject> {
                            
                            
                            if let weekTemp = dailyTemp["data"] as? [Dictionary<String, AnyObject>] {

                                //Store MAX, MIN temp and icons for all week
                                
                                /*
                                    THIS IS GETTING WEATHER DATA INCLUDING YESTERDAYS 
                                    THUS START FROM 1 INORDER TO GET TOMORROWS TEMP AND FURTHER ON
                                 */
                                for day in 1 ..< weekTemp.count {
                                    self._temperatureMax.append("\(Int(round(weekTemp[day]["temperatureMax"]! as! Double)))")
                                    self._temperatureMin.append("\(Int(round(weekTemp[day]["temperatureMin"]! as! Double)))")
                                    
                                    //icons
                                    self._icon.append(weekTemp[day]["icon"]! as! String)
                                }
                            }
                        }
                    }
                    
                    //Get weather current summary
                    if let weatherSummary = currentWeather["summary"] as? String{
                        self._tempDescription = weatherSummary
                    }
                    
                    // Precipitation probability
                    if let precipProb = currentWeather["precipProbability"] as? Double {
                        self._precipProbability = "\(Int(round(precipProb*100)))%"
                    }
                    
                    // Humidity level
                    if let humid = currentWeather["humidity"] as? Double{
                        self._humidity = "\(Int(round(humid*100)))%"
                    }
                    
                    //Wind Speed
                    if let wind = currentWeather["windSpeed"] as? Double {
                        self._windSpeed = "\(Int(round(wind)))"
                    }
                    
                }
                
                //print(" City: \(self._cityName)\n temperature: \(self._temperature)\n temp MAX: \(self._temperatureMax[0])\n temp MIN: \(self._temperatureMin[0])\n time: \(self._time), date: \(self._date)\n temp DESC: \(self._tempDescription)\n humidity: \(self._humidity)\n wind Speed: \(self._windSpeed)\n percip Prob: \(self._precipProbability)\n icon: \(self._icon)")
            }
            
            completed()
        }
        
    }
    
    func setupTimeByTimezone(date: NSDateComponents) -> String {
        var am_pm: String!
        
        if date.hour > 11 {
            am_pm = "PM"
        } else {
            am_pm = "AM"
        }
        
        date.hour = date.hour%12
        
        if date.hour == 0 {
            date.hour = 12
        }
        
        return am_pm
    }
    
    func getWeekDays(date: NSDateComponents){
        
        _daysOfWeek = []
        let daysOfWeekNames = ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"]
        
        //Set rest of the days
        for x in date.weekday ..< date.weekday + 7 {
            
            let result = x%8
            if result != 0 {
                _daysOfWeek.append(daysOfWeekNames[result-1])
            }
        }
 
    }

    
    // Getters
    var cityName: String {
        get {
            if _cityName == nil {
                return ""
            }
            return _cityName
        }
    }
    
    var longitude: String {
        get{
            if _longitude == nil{
                return ""
            }
            return _longitude
        }
    }
    
    var latitude: String {
        get{
            if _latitude == nil{
                return ""
            }
            return _latitude
        }
    }
    
    var temperature: String {
        get{
            if _temperature == nil{
                return ""
            }
            return _temperature
        }
    }
    
    var temperatureMin: [String]{
        get{
            if _temperatureMin == nil{
                return [""]
            }
            return _temperatureMin
        }
    }
    
    var temperatureMax: [String]{
        get{
            if _temperatureMax == nil{
                return [""]
            }
            return _temperatureMax
        }
    }
    
    var time: String{
        get{
            if _time == nil{
                return ""
            }
            return _time
        }
    }
    
    var timezone: String {
        get{
            if _timezone == nil{
                return ""
            }
            return _timezone
        }
    }
    
    var daysOfWeek: [String] {
        get{
            if _daysOfWeek == nil {
                return []
            }
            return _daysOfWeek
        }
    }
    
    var date: String{
        get{
            if _date == nil{
                return ""
            }
            return _date
        }
    }
    
    var tempDescription: String{
        get{
            if _tempDescription == nil{
                return ""
            }
            return _tempDescription
        }
    }
    
    var icon: [String]{
        get{
            if _icon == nil{
                return [""]
            }
            return _icon
        }
    }
    
    var humidity: String{
        get{
            if _humidity == nil{
                return ""
            }
            return _humidity
        }
    }
    
    var windSpeed: String{
        get{
            if _windSpeed == nil{
                return ""
            }
            return _windSpeed
        }
    }
    
    var precipProbability: String{
        get{
            if _precipProbability == nil{
                return ""
            }
            return _precipProbability
        }
    }
    
    
}
