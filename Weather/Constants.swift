//
//  Constants.swift
//  Weather
//
//  Created by Juan Ramirez on 9/8/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

// Open Weather API
let openweatherURL_BASE = "http://api.openweathermap.org/data/2.5/forecast/"
let openweatherAPP_KEY = "&APPID=ee2dbd152f4c391d2a569d96dfedd84f"
let openweatherURL_CITY = "city?id="
let openweatherSampleCity = "http://api.openweathermap.org/data/2.5/forecast/city?id=524901&APPID=ee2dbd152f4c391d2a569d96dfedd84f"

/* Forecast.io API
 *
 * Format: https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE
 
 https://api.darksky.net/forecast/32366803bff06be6bd66b3df8ef634ce/37.8267,-122.4233
 35.2229,-80.8380   -> Charlotte
 */
let forecastURL_BASE = "https://api.darksky.net/forecast/" //https://api.forecast.io/forecast/
let forecastAPP_KEY = "32366803bff06be6bd66b3df8ef634ce/"
let forecaseSampleCity = "https://api.darksky.net/forecast/32366803bff06be6bd66b3df8ef634ce/37.7749295,-122.419155"



typealias DownloadComplete = () -> ()



