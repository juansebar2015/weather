//
//  ViewController.swift
//  Weather
//
//  Created by Juan Ramirez on 9/8/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
//import SwiftyJSON

class ViewController: UIViewController, CLLocationManagerDelegate { //, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var locationManager = CLLocationManager()
    //@IBOutlet weak var collectionView: UICollectionView!
    
    
    var weather: Weather!   //[Weather]!
    var cityName: String!
    var latitude: String!
    var longitude: String!
    
    var userEnteredNewCity: Bool = false
    
    // Stores NSDefault city data
    var usersCityName: String!
    var usersLatitude: String!
    var usersLongitude: String!
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var currentWeatherDescLabel: UILabel!
    @IBOutlet weak var currentDayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var rainPercentLabel: UILabel!
    @IBOutlet weak var humidPercentLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var currentMaxTempLabel: UILabel!
    @IBOutlet weak var currentMinTempLabel: UILabel!
    
    @IBOutlet weak var weekDayOneLabel: UILabel!
    @IBOutlet weak var weekDayOne: UIImageView!
    @IBOutlet weak var weekDayOneMaxTemp: UILabel!
    @IBOutlet weak var weekDayOneMinTemp: UILabel!
    @IBOutlet weak var weekDayTwoLabel: UILabel!
    @IBOutlet weak var weekDayTwo: UIImageView!
    @IBOutlet weak var weekDayTwoMaxTemp: UILabel!
    @IBOutlet weak var weekDayTwoMinTemp: UILabel!
    @IBOutlet weak var weekDayThreeLabel: UILabel!
    @IBOutlet weak var weekDayThree: UIImageView!
    @IBOutlet weak var weekDayThreeMaxTemp: UILabel!
    @IBOutlet weak var weekDayThreeMinTemp: UILabel!
    @IBOutlet weak var weekDayFourLabel: UILabel!
    @IBOutlet weak var weekDayFour: UIImageView!
    @IBOutlet weak var weekDayFourMaxTemp: UILabel!
    @IBOutlet weak var weekDayFourMinTemp: UILabel!
    @IBOutlet weak var weekDayFiveLabel: UILabel!
    @IBOutlet weak var weekDayFive: UIImageView!
    @IBOutlet weak var weekDayFiveMaxTemp: UILabel!
    @IBOutlet weak var weekDayFiveMinTemp: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if let storedCity = NSUserDefaults.standardUserDefaults().valueForKey("cityName") as? String{
            
            if let storedLatitude = NSUserDefaults.standardUserDefaults().valueForKey("latitude") as? String{
                
                if let storedLongitude = NSUserDefaults.standardUserDefaults().valueForKey("longitude") as? String{
                    usersCityName = storedCity
                    usersLatitude = storedLatitude
                    usersLongitude = storedLongitude
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
        
        /**
        
         Logic to have many cities to display the weather
        
        
        */
//        if weather == nil {
//            weather = []
//        }
//        
//        // Entered new city
//        if userEnteredNewCity {
//            
//            userEnteredNewCity = false
//                
//            //Just append the new city
//            weather.append(Weather(name: cityName, lat: latitude, lon: longitude))
//            getWeatherData()
//            
//        } else{
//            
//            //Check if there is any input
//            if weather.count > 0 {
//                getWeatherData()
//            } else {
//                //Get Weather of users location
//                weather.append(Weather(name: "Charlotte", lat: "37.33233141", lon: "-122.0312186"))
//                getWeatherData()
//            }
//            //getWeatherData()
//        }

        //weather = Weather(name: "San Francisco", lat: "37.33233141", lon: "-122.0312186")
        //getWeatherData()
        
        // get weather for city entered
        if userEnteredNewCity {
            weather = Weather(name: cityName, lat: latitude, lon: longitude)
            
            getWeatherData()
            saveWeather(weather)
            //collectionView.reloadData()
        }
        
        // No location entered thus get users savedLocation
        if weather == nil {
            
            //Get users location
            if usersCityName != nil {
                if usersLatitude != nil && usersLongitude != nil {
                    //weather = Weather(name: usersCityName, lat: usersLatitude, lon: usersLongitude)
                    weather = Weather(name: usersCityName, lat: usersLatitude, lon: usersLongitude) //Weather(name: "Charlotte", lat: "35.2229", lon: "-80.8380")
                    getWeatherData()
                }
            } else {
                // Go to a default location
                weather = Weather(name: "Charlotte", lat: "35.2229", lon: "-80.8380")
                getWeatherData()
            }
        }
        
    }
    
    func getWeatherData(){
        
        if let weatherCity = weather {
            
            weatherCity.getWeatherData{ ()->() in
                
                self.updateUI()
            }
        }
    }
    
    func updateUI(){

        if let currWeather = weather {
            
            //daysOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thur", "Sat"]
            
            cityNameLabel.text = currWeather.cityName
            currentWeatherImage.image = UIImage(named: currWeather.icon[0])
            currentWeatherLabel.text = "\(currWeather.temperature)°"
            currentWeatherDescLabel.text = currWeather.tempDescription
            currentDayLabel.text = "\(currWeather.daysOfWeek[0])"
            timeLabel.text = currWeather.time
            rainPercentLabel.text = currWeather.precipProbability
            humidPercentLabel.text = currWeather.humidity
            windSpeedLabel.text = "\(currWeather.windSpeed) MPH"
            currentMaxTempLabel.text = "\(currWeather.temperatureMax[0])°"
            currentMinTempLabel.text = "\(currWeather.temperatureMin[0])°"
            
            //Fill the rest for the following week
            //Day 1
            weekDayOneLabel.text = currWeather.daysOfWeek[1]
            weekDayOne.image = UIImage(named: currWeather.icon[1])
            weekDayOneMaxTemp.text = "\(currWeather.temperatureMax[1])°"
            weekDayOneMinTemp.text = "\(currWeather.temperatureMin[1])°"
            
            //Day 2
            weekDayTwoLabel.text = currWeather.daysOfWeek[2]
            weekDayTwo.image = UIImage(named: currWeather.icon[2])
            weekDayTwoMaxTemp.text = "\(currWeather.temperatureMax[2])°"
            weekDayTwoMinTemp.text = "\(currWeather.temperatureMin[2])°"
            
            //Day 3
            weekDayThreeLabel.text = currWeather.daysOfWeek[3]
            weekDayThree.image = UIImage(named: currWeather.icon[3])
            weekDayThreeMaxTemp.text = "\(currWeather.temperatureMax[3])°"
            weekDayThreeMinTemp.text = "\(currWeather.temperatureMin[3])°"
            
            //Day 4
            weekDayFourLabel.text = currWeather.daysOfWeek[4]
            weekDayFour.image = UIImage(named: currWeather.icon[4])
            weekDayFourMaxTemp.text = "\(currWeather.temperatureMax[4])°"
            weekDayFourMinTemp.text = "\(currWeather.temperatureMin[4])°"
            
            //Day 5
            weekDayFiveLabel.text = currWeather.daysOfWeek[5]
            weekDayFive.image = UIImage(named: currWeather.icon[5])
            weekDayFiveMaxTemp.text = "\(currWeather.temperatureMax[5])°"
            weekDayFiveMinTemp.text = "\(currWeather.temperatureMin[5])°"
            
        }
    }
    
    func saveWeather(weather: Weather) {
        
        NSUserDefaults.standardUserDefaults().setObject(weather.cityName, forKey: "cityName")
        NSUserDefaults.standardUserDefaults().setObject(weather.latitude, forKey: "latitude")
        NSUserDefaults.standardUserDefaults().setObject(weather.longitude, forKey: "longitude")
        
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus){
        if status == .AuthorizedWhenInUse{
            
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // Get the first location and stop updating
        if let loc = locations.first {
            
            //Save the location and check if its a different city 
            
            //Then check based on the time it was retreived
            
            usersLatitude = "\(loc.coordinate.latitude)"
            usersLongitude = "\(loc.coordinate.longitude)"
            
            //Get city name
            //reverseGeocodeCoordinate(loc.coordinate)
            
            print("Latitude: \(usersLatitude)")
            print("longitude: \(usersLongitude)")
            
            locationManager.stopUpdatingLocation()
        }
        
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D){
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            
            if let address = response?.firstResult() {
                self.usersCityName = address.locality
                
                print("City name: \(self.usersCityName)")
            }
            
        }
    }
    
//    //Collection View Delegate methods
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        return 1
//    }
//    
//    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        
//        if userEnteredNewCity {
//            return 5    // For five days of week
//        } else {
//            return 5
//        }
//    }
//    
//    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        
//        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("WeekDayCell", forIndexPath: indexPath) as? WeekDayCell {
//            
//            if weather != nil {
//                if weather.temperature == "" {
//                    
//                    cell.configCell(weather, dayNum: (indexPath.row+1))
//                    return cell
//                }
//            }
//            
//        }
//        
//        return UICollectionViewCell()
//        
//    }
}




























































